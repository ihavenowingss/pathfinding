package mygame;

import com.jme3.asset.AssetManager;
import com.jme3.bounding.BoundingVolume;
import com.jme3.collision.CollisionResults;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import com.jme3.scene.shape.Sphere;

/**
 * ANode is a class that adapts the standard "Node" to function inside this engine
 * In short, it acts like a node with a spatial and has f,g and h values that are needed
 * 
 * @author Bruno
 */
public class ANode {
    private Node node;
    private Vector3f location;
    private Spatial spat;
    private AssetManager assetManager;
    int f = 0;
    int g = 0;
    float h = 0;
    private ANode parent;
    
    public ANode(AssetManager assetManager, Vector3f location){
        this.assetManager = assetManager;
        node = new Node(location.toString()+"A*");
        Sphere sphere = new Sphere(3, 3, 0.4f);
        spat = new Geometry("BOOM!", sphere);
        Material mark_mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mark_mat.setColor("Color", ColorRGBA.Red);
        spat.setMaterial(mark_mat);
        spat.setLocalTranslation(location);
        //node.setLocalTranslation(location);
        node.attachChild(spat);
    }
    /**
     * Internal use only
     */
    public ANode(){}
    
    /**
     * Not in use since it seems that it's better that sceneHandler takes care of checking for valids
     * @param bv
     * @return 
     */
    public boolean isValid(BoundingVolume bv){
        CollisionResults results = new CollisionResults();
        spat.collideWith(bv, results);
        Material mark_mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mark_mat.setColor("Color", ColorRGBA.Blue);
        
        if (results.size() > 0) {
            spat.setMaterial(mark_mat);
            
        }
        return true;
    }
    
    public Node getNode(){
        return node;
    }
    public Spatial getSpatial(){
        return spat;
    }
    
    public void calculateFinalCost(){
        f = (int) (g+h);
    }
    /**
     * This is used to update the data of nodes as they are inspected
     * @param currentNode is the node that we are currently inspecting and are gonna send to the closed list after these checks
     * @param cost how much this movement costs, this is because moving diagonaly generally costs a square root of 2 more then any other movement on a grid
     */  
    public void setData(ANode currentNode, int cost){
        setParent(currentNode);
        g = currentNode.g + cost;
        calculateFinalCost();
    }
    
    /**
     * Checks if there is a better path, if there is it calls the "setData" function
     * @param currentNode 
     * @param cost
     * @return returns flase if the path currently assigned is better then the path being checked
     */
    public boolean checkForBetter(ANode currentNode, int cost){
        int newG = currentNode.g + cost;
        if(newG < g){
            setData(currentNode, cost);
            return true;
        }
        return false;
    }
    
    public ANode getPerant(){
        return parent;
    }
    public void setParent(ANode p){
        this.parent = p;
    }
}