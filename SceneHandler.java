package mygame;

import com.jme3.asset.AssetManager;
import com.jme3.bounding.BoundingVolume;
import com.jme3.collision.CollisionResults;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import com.jme3.scene.Spatial;
import java.util.List;

/**
 * Class used to handle scene objects and work with pathfinding
 * @author Bruno
 */
public class SceneHandler {
    private Spatial spatial;
    private Vector3f offset;
    private AssetManager assetManager;
    private String Directory;
    private Node node;
    
    /**
     * 
     * @param directory Directory of the model used for scene
     * @param offset Offset of scene, for 0 use Vector3f.ZERO
     * @param assetManager assetmanager
     */
    public SceneHandler(String directory, Vector3f offset, AssetManager assetManager){
        this.assetManager = assetManager;
        this.offset = offset;
        spatial = assetManager.loadModel(directory+".j3o");
        node = new Node(directory);
        node.attachChild(spatial);
    }
    
    public Node getNode(){
        return node;
    }
    public Spatial getSpatial()
    {
        return spatial;
    }
    /**
     * Adds nodes that are not coliding with the scene to the valid list of the APopulate
     * @param apop APopulate object
     */
    public void checkValids(APopulate apop){
        List<ANode> list = apop.getList();
        Material mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        mat.setColor("Color", ColorRGBA.Blue);
                
        for(int i = 0; i < list.size(); i++){
                //System.out.println("it's true");
                BoundingVolume bv = list.get(i).getSpatial().getWorldBound();
                bv.setCenter(list.get(i).getSpatial().getLocalTranslation());
                CollisionResults results = new CollisionResults();
                
                spatial.collideWith(list.get(i).getSpatial().getWorldBound(), results);
                if (results.size() > 0) {
                    System.out.println(results.toString());
                    list.get(i).getSpatial().setMaterial(mat);
                }else{
                    apop.getValidList().add(list.get(i));
                }
                
            }
    }
}