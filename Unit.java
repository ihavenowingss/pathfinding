package mygame;

import com.jme3.asset.AssetManager;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Spatial;
import java.util.List;

/**
 * Unit class, currently a static object that is the start of the path,
 * Will eventually be an object that can move along a generated path
 * @author Bruno
 */
public class Unit {
    private Spatial spat;
    private Vector3f location;
    private AssetManager assetManager;
    public String Directory;
    
    public Unit(String Directory, Vector3f loc, AssetManager assetManager){
        this.Directory = Directory;
        this.location = loc;
        this.assetManager = assetManager;
        
        spat = assetManager.loadModel(Directory + ".j3o");
        spat.setLocalTranslation(location);
        
    }
    
    public Spatial getSpatial(){
        return spat;
    }
    /**
     * This method colors the node that is closest to the Unit
     * Just for representation use
     * @param apop
     * @return 
     */
    public ANode colorClosest(APopulate apop){
        List<ANode> list = apop.getValidList();
        ANode closest = list.get(0);
        float dist = 10;
        int i;
        Material grn = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
        grn.setColor("Color", ColorRGBA.Green);
        for(i = 0; i < list.size(); i++){
            if(list.get(i).getSpatial().getLocalTranslation().distance(location)<dist){
                closest = list.get(i);
                dist = list.get(i).getSpatial().getLocalTranslation().distance(location);
            }
            
        }
        closest.getSpatial().setMaterial(grn);
        return closest;
        
    }
}