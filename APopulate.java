package mygame;

import com.jme3.asset.AssetManager;
import com.jme3.bounding.BoundingVolume;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector3f;
import com.jme3.scene.Node;
import static java.lang.Math.sqrt;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

/**
 * This class is used to populate the map with nodes, find the path and claculate the H value
 * @author Bruno
 */
public class APopulate {
    
        private ArrayList<ANode> list = new ArrayList<>();
        private Node mainNode = new Node("APopNode");
        private AssetManager assetManager;
        private ANode aNodeMain;
        private ArrayList<ANode> validNodes = new ArrayList<>();
        private ANode target;
        private float nearNodeDistance;
        //List<ANode> openList;
        List<ANode> closedList;
        ANode currentNode;
        private PriorityQueue<ANode> openList;
        
        public APopulate(AssetManager assetmanager){
            this.assetManager =assetmanager;
            openList = new PriorityQueue<>(new Comparator<ANode>(){
                @Override
                public int compare (ANode node0, ANode node1){
                    return Integer.compare(node0.f, node1.f );
                }
            });
            
        }
        
        /**
         * Places ANode-s all over the map to create a grid of nodes for pathfinding
         * @param edge1 First corner
         * @param edge2 Second corner
         * @param sections This integer defines how many nodes there will be, your memory might not allow for very high numbers
         */
        public void populate(Vector3f edge1, Vector3f edge2, int sections){
            float xStart = edge1.x;
            float zStart = edge1.z;
            float xEnd = edge2.x;
            float zEnd = edge2.z;
            float hight = edge1.y;
            
            float xLenght = xEnd-xStart;
            float zLenght = zEnd-zStart;
            
            float xIncrement = xLenght/sections;
            float zIncrement = zLenght/sections;
            
            float x = xIncrement * xIncrement * 2;
            nearNodeDistance = (float) sqrt(x);
            
            for(float fx = xStart; fx <= xEnd; fx += xIncrement){
                for(float fz = zStart; fz <= zEnd; fz += zIncrement){
                    aNodeMain = new ANode(assetManager,new Vector3f(fx, hight, fz));
                    list.add(aNodeMain);
                    mainNode.attachChild(aNodeMain.getNode());
                    
                }
            }
            
        }
        
        /**
         * Generally not in use because with testing it seems that it's better for sceneHandler to check for valid nodes
         * @param bv 
         */
        public void checkValids(BoundingVolume bv){
            for(int i = 0; i < list.size(); i++){
                //System.out.println("it's true");
                if(list.get(i).isValid(bv))
                    System.out.println("it's true");
                
            }
        }
        
       
        public Node getNode(){
            return mainNode;
        }
        public List<ANode> getList(){
            return list;
        }
        public List<ANode> getValidList(){
            return validNodes;
        }
        public ANode getTarget(){
            return target;
        }
        public void setTarget(ANode targ){
            this.target = targ;
        }
        
        /**
         * This method uses the starting node to find the path
         * @param start
         * @return 
         */
        public List<ANode> findPath(ANode start){
            // First we need to claculate h values, another solution would be to calulate it when h becomes relevant, but this system works too
            for(int i = 0; i < validNodes.size(); i++){
                calculateH (validNodes.get(i));
            }
            // Initialize the closed list
            closedList = new ArrayList<>();
            // Add the starting node on the openList priority list
            openList.add(start);
            while (!openList.isEmpty()){
                // The poll() method removes the object with the index of 0 and returns it
                currentNode = openList.poll();
                // That node goes into the closed list, so we don't accidentally re check the node
                closedList.add(currentNode);
                // If we reached the end, color the path and return it
                if(currentNode == getTarget()){
                    Material m1 = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
                    m1.setColor("Color", ColorRGBA.Yellow);
                    List<ANode> path = getPath(currentNode);
                    for(int i = 0; i<path.size(); i++){
                        path.get(i).getSpatial().setMaterial(m1);
                    }
                }else {
                    //If we didn't reach the end, add all nodes around the current node to the open list
                    addNodesWithinRange();
                }
                
            }
            return getPath(currentNode);
        }
        /**
         * A simple way to put the path onto a list
         * @param currentNode
         * @return list of ANode-s that represent the path 
         */
        private List<ANode> getPath(ANode currentNode){
            List<ANode> path = new ArrayList<>();
            path.add(currentNode);
            ANode parent;
            while((parent = currentNode.getPerant()) != null){
                path.add(0, parent);
                currentNode = parent;
            }
            return path;
        }
        
        /**
         * Finds nodes that around the node that is currently in focus and tries to add them on the open list
         */
        private void addNodesWithinRange(){
            for(int i = 0; i<validNodes.size(); i++){
                
                    if(validNodes.get(i) != currentNode){
                    
                        if(currentNode.getSpatial().getLocalTranslation().distance(
                                validNodes.get(i).getSpatial().getLocalTranslation())<=nearNodeDistance){
                            
                            checkNode(validNodes.get(i), getCost(validNodes.get(i)));
                        }
                    
                    }
                
            }
        }
        /**
         * This method gives the proper cost for that movement
         * Since there is only vertical/horizontal movement and diagonal, it 
         * only has to determine wich one that is and return an appropriate value
         * @param a
         * @return cost
         */
        public int getCost(ANode a){
            if(currentNode.getSpatial().getLocalTranslation().distance(
                                a.getSpatial().getLocalTranslation())<nearNodeDistance)
                return 10;
            return 14;
        }
        
        /**
         * Here we check if the adjacent nodes are in the open list or the closed
         * list and take and add them to open list if so is needed
         * More explenation in the method
         * @param adjnode
         * @param cost 
         */
        public void checkNode(ANode adjnode, int cost){
            if (!(closedList.contains(adjnode))){
                if(!openList.contains(adjnode)){
                    //if the node is not on any of the lists, it's being checked
                    //for the first time, this means it has to go on the open list
                    adjnode.setData(currentNode, cost);
                    openList.add(adjnode);
                }else{
                    //if it's on the closed list we need to check if the current
                    //path is better then path previusly made
                    boolean b = adjnode.checkForBetter(currentNode, cost);
                    if(b){
                        //if the new path is better it is assigned to the node
                        //and the node is removed and added from the open list
                        //This is needed because it's a priority list
                        openList.remove(adjnode);
                        openList.add(adjnode);
                    }
                }
            }
        }
        
        /**
         * Methods does a simple estimate of the remaining distance
         * Actually knowing the remaining distance is impossible without finding the path
         * @param a 
         */
        public void calculateH(ANode a){
            a.h = a.getSpatial().getLocalTranslation().distance(target.getSpatial().getLocalTranslation()) * 10;//multiplied by 10 se we can keep using Integers for easier conversions later on
        }
        
        /**
         * Recolors the nodes red when we need to find a new path
         */
        public void colorRed(){
            Material m1 = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
            m1.setColor("Color", ColorRGBA.Red);
            for(int i = 0; i < validNodes.size(); i++){
                
                validNodes.get(i).getSpatial().setMaterial(m1);
            }
        }
        
        
    
}
