package mygame;

import com.jme3.app.SimpleApplication;
import com.jme3.collision.CollisionResult;
import com.jme3.collision.CollisionResults;
import com.jme3.font.BitmapText;
import com.jme3.input.KeyInput;
import com.jme3.input.MouseInput;
import com.jme3.input.controls.ActionListener;
import com.jme3.input.controls.KeyTrigger;
import com.jme3.input.controls.MouseButtonTrigger;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Ray;
import com.jme3.math.Vector3f;
import com.jme3.renderer.RenderManager;
import com.jme3.scene.Geometry;
import com.jme3.scene.Node;
import com.jme3.scene.shape.Sphere;
import java.util.List;

/**
 * Main class
 * @author Bruno
 */
public class Main extends SimpleApplication{

    
    private Geometry mark;
    private Node shootables;
    private SceneHandler scene;
    APopulate apop;
    private ANode start;
    
    public static void main(String[] args) {
        Main app = new Main();
        app.start();
    }

    /**
     * Setting up the whole layout
     */
    @Override
    public void simpleInitApp() {
        System.out.println("hi from main");
        flyCam.setMoveSpeed(40f);
        shootables = new Node("Shootables");
        rootNode.attachChild(shootables);
        scene = new SceneHandler("Models/DefaultMap",Vector3f.ZERO,assetManager);
        shootables.attachChild(scene.getNode());
        initMark();
        initKeys();
        initCrossHairs();
        apop = new APopulate(assetManager);
        apop.populate(new Vector3f(-10,1,-10), new Vector3f(30,1,30), 50);
        rootNode.attachChild(apop.getNode());
        scene.checkValids(apop);
        Unit uni = new Unit("Models/Chevy", new Vector3f(2,0,0), assetManager);
        start = uni.colorClosest(apop);
        rootNode.attachChild(uni.getSpatial());

    }

    @Override
    public void simpleUpdate(float tpf) {
        //TODO: add update code
    }

    @Override
    public void simpleRender(RenderManager rm) {
        //TODO: add render code
    }
    /**
     * Setting LMB and Space to be a "Shoot" button
     * Movement is set up in the flyCam definition that is used by default (good enough for this)
     */
    private void initKeys() {
    inputManager.addMapping("Shoot",
      new KeyTrigger(KeyInput.KEY_SPACE), // trigger 1: spacebar
      new MouseButtonTrigger(MouseInput.BUTTON_LEFT)); // trigger 2: left-button click
    inputManager.addListener(actionListener, "Shoot");
  }
  /** Defining the "Shoot" action: Determine what was hit and how to respond. */
  private ActionListener actionListener = new ActionListener() {

      /**
       * This seems quite lenghty but I'll explain it step by step
       * The function automaticly triggers when any input is triggered,
       * We have to check what action it is (in our case there is only the "shoot" function)
       * 
       * @param name Action name, defined in the "initKeys()" function
       * @param keyPressed This allows us to play with activation, in this case it's set up to trigger an action when the key is released
       * @param tpf Time per frame, this helps us with certan actions, not the ones that we need in this case however
       */
    public void onAction(String name, boolean keyPressed, float tpf) {
      if (name.equals("Shoot") && !keyPressed) {
        // 1. Reset results list.
        CollisionResults results = new CollisionResults();
        // 2. Aim the ray from cam loc to cam direction.
        Ray ray = new Ray(cam.getLocation(), cam.getDirection());
        // 3. Collect intersections between Ray and Shootables in results list.
        shootables.collideWith(ray, results);
        // 4. Print the results
        System.out.println("----- Collisions? " + results.size() + "-----" );
        
        // 5. Use the results (we mark the hit object)
        if (results.size() > 0) {
          // The closest collision point is what was truly hit:
          CollisionResult closest = results.getClosestCollision();
          // We mark the hit with a red dot.
          mark.setLocalTranslation(closest.getContactPoint());
          rootNode.attachChild(mark);
        } else {
          // No hits? Then remove the red mark.
          rootNode.detachChild(mark);
        }
        // 6. If there was a hit we search the ANodes to find the closest ANode and set that as the target
        if(rootNode.hasChild(mark)){
            for(int i = 0; i < apop.getValidList().size(); i++){
                // Coloring the closest node in black is useless in a scenario where the path is found
                // This means that if target is unreachable the target will be colored black but no path will be displayed
                float dist = 10;
                Material grn = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
                grn.setColor("Color", ColorRGBA.Black);
                for(i = 0; i < apop.getValidList().size(); i++){
                if(apop.getValidList().get(i).getSpatial().getLocalTranslation().distance(mark.getLocalTranslation())<dist){
                    if(apop.getTarget()!=null){
                        Material gr = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
                        gr.setColor("Color", ColorRGBA.Red);
                        apop.getTarget().getSpatial().setMaterial(gr);
                    }
                    dist = apop.getValidList().get(i).getSpatial().getLocalTranslation().distance(mark.getLocalTranslation());
                    apop.setTarget(apop.getValidList().get(i));
            }
            
        }
        apop.getTarget().getSpatial().setMaterial(grn);
        
            }
            // 7. Remove any previus paths to avoid confusion and find the shortest path to the new target node
            apop.colorRed();
            // Now we have a list of nodes that we can use to move our object, or we can try to optimize it
            List<ANode> path = apop.findPath(start);
        }
        
      }
    }
  };
  /**
   * Making the sphere object that we will put on the surface we click on
   */
  protected void initMark() {
    Sphere sphere = new Sphere(30, 30, 0.2f);
    mark = new Geometry("BOOM!", sphere);
    Material mark_mat = new Material(assetManager, "Common/MatDefs/Misc/Unshaded.j3md");
    mark_mat.setColor("Color", ColorRGBA.Red);
    mark.setMaterial(mark_mat);
  }
  /**
   * Making a + in the middle of the screen
   */
  protected void initCrossHairs() {
    setDisplayStatView(false);
    guiFont = assetManager.loadFont("Interface/Fonts/Default.fnt");
    BitmapText ch = new BitmapText(guiFont, false);
    ch.setSize(guiFont.getCharSet().getRenderedSize() * 2);
    ch.setText("+"); 
    ch.setLocalTranslation( 
      settings.getWidth() / 2 - ch.getLineWidth()/2,
      settings.getHeight() / 2 + ch.getLineHeight()/2, 0);
    guiNode.attachChild(ch);
  }
}

